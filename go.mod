module modernc.org/zappy

go 1.18

require (
	github.com/golang/snappy v0.0.3
	modernc.org/internal v1.1.0
)

require (
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	modernc.org/mathutil v1.6.0 // indirect
)
